import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  profileForm: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.profileForm = this.fb.group({
      firstName: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50), Validators.pattern('^[a-zA-Z]{1,}$')]],
      lastName: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50), Validators.pattern('^[a-zA-Z]{1,}$')]],
      email: ['', [Validators.required, Validators.email, Validators.maxLength(50)]],
      address: this.fb.group({
        street: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(150), Validators.pattern('^[a-zA-Z].*[\s\.]*$')]],
        city: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(20), Validators.pattern('^[a-zA-Z]{1,}$')]],
        state: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(20), Validators.pattern('^[a-zA-Z]{1,}$')]],
        pin: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(10), Validators.pattern('^[0-9]{1,}$')]]
      }),
    });
  }

  get loginFormControl() {
    return this.profileForm.controls;
  }

  get streetControl() {
    //console.log(this.profileForm.get('address').value)
    return this.profileForm.get('address.street');
  }

  get cityControl() {
    return this.profileForm.get('address.city');
  }

  get stateControl() {
    return this.profileForm.get('address.state');
  }

  get pinControl() {
    return this.profileForm.get('address.pin');
  }

  onSubmit() {
    // console.log(this.loginFormControl.address.value)
    console.log(this.profileForm.value);
    alert('Form Submitted Successfully');
  }

}
